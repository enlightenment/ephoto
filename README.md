Ephoto - A comprehensive image viewer written using the core EFL.

## Ephoto

Ephoto is an image viewer and editor written using the
[Enlightenment Foundation Libraries](http://www.enlightenment.org).  It
focuses on simplicity and ease of use, while taking advantage of the speed
and small footprint the EFL provide. You can view Ephoto's project page on the Enlightenment wiki located at:
<https://www.enlightenment.org/about-ephoto>.  Ephoto's source can
be found in the Enlightenment git repository:
<https://git.enlightenment.org/enlightenment/ephoto>

## Features

Ephoto's features include:

* Browsing the filesystem and displaying images in an easy to use grid view.
* Browsing images in a single image view format.
* Viewing images in a slideshow.
* Editing your image with features such as cropping, auto enhance, blurring, sharpening, brightness/contrast/gamma adjustments, hue/saturation/value adjustments, and color level adjustment.
* Applying artistic filters to your image such as black and white and old photo.
* Drag And Drop along with file operations to easy maintain your photo directories.

## Screenshots

Here are a few example screenshots of Ephoto in action:

![Ephoto Thumbnails](/data/screenshots/ephoto1.png "Ephoto Thumbnail View")

![About Ephoto](/data/screenshots/ephoto2.png "About Ephoto")

![Ephoto Single View](/data/screenshots/ephoto3.png "Ephoto Single Image View")
